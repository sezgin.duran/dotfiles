## Personal dotfiles

Contains:

* Vim configuration
* (oh-my-) zsh configuration
* Aliases

## License

[WTFPL](https://gitlab.com/marjinal1st/dotfiles/blob/master/LICENSE.txt)